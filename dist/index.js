/*
 * Blackbox API for user management, authentication and authroisation.
 * TODO Convert the below procedural approach to fully OO.
 */
'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_rules_utils_1 = require("blackbox-rules-utils");
var blackbox_database_1 = require("blackbox-database");
var blackbox_rules_service_1 = __importDefault(require("blackbox-rules-service"));
var blackbox_root_service_1 = __importDefault(require("blackbox-root-service"));
var cors_1 = __importDefault(require("cors"));
var securityMiddleware_1 = require("./src/securityMiddleware");
var blackbox_config_1 = require("blackbox-config");
var config = blackbox_config_1.loadConfig();
if (config.databases && config.databases.user) {
    blackbox_database_1.configureDatabase(config.databases.user); // Must be called before any database decorations.
}
var fs = require('fs'), path = require('path'), http = require('http'), glob = require('glob'), express = require('express'), oas3Tools = require('oas3-tools'), jsyaml = require('js-yaml'), serverPort = 8088;
var app = express();
// Load all files to ensure decorations are read for IOC:
glob
    .sync(__dirname + "/*src/**/*.js")
    .forEach(function (file) {
    require(path.resolve(file));
});
var AppInitialiser = /** @class */ (function () {
    function AppInitialiser() {
    }
    AppInitialiser.prototype.createRuleBase = function () {
        return new blackbox_rules_utils_1.DefaultRuleBase();
    };
    AppInitialiser.prototype.getOasDoc = function () {
        if (!this.swaggerDoc) {
            var spec = fs.readFileSync(path.join(__dirname, 'api/openapi.yaml'), 'utf8');
            this.swaggerDoc = jsyaml.safeLoad(spec);
        }
        return this.swaggerDoc;
    };
    AppInitialiser.prototype.init = function () {
        blackbox_rules_service_1.default();
        blackbox_root_service_1.default();
        // swaggerRouter configuration
        var options = {
            swaggerUi: path.join(__dirname, '/openapi.json'),
            controllers: path.join(__dirname, './gensrc/controllers'),
            useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
        };
        // Initialize the Swagger middleware
        oas3Tools.initializeMiddleware(this.getOasDoc(), function (middleware) {
            app.use(cors_1.default({
                "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS"
            }));
            app.options('/users', function (req, res) {
                console.log('test');
            });
            app.get('/login.html', function (req, res) {
                res.sendFile(path.join(__dirname + '/login.html'));
            });
            app.get('/favicon.ico', function (req, res) {
                res.sendFile(path.join(__dirname + '/favicon.ico'));
            });
            app.use(securityMiddleware_1.authenticationMiddleware({ ignore: ['/authentication', '/authentication/', '/'] }));
            // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
            app.use(middleware.swaggerMetadata());
            // Validate Swagger requests
            app.use(middleware.swaggerValidator());
            app.use(securityMiddleware_1.authorisationMiddleware({ ignore: ['/authentication', '/authentication/', '/'] }));
            // Handle security errors (before swagger handles it):
            app.use(clientErrorMiddleware);
            // Route validated requests to appropriate controller
            app.use(middleware.swaggerRouter(options));
            // Handle client errors thrown by service implementations:
            app.use(clientErrorMiddleware);
            // Serve the Swagger documents and Swagger UI
            app.use(middleware.swaggerUi());
            // TODO better error handler or fix the inbuilt one that the htt server is providing
            // app.use(function errorHandler(err:any, req:any, res:any, next:any) {
            //   // default to 500 server error
            //   return res.status(500).json({ message: err.message });
            // })
            // Start the server
            http.createServer(app).listen(serverPort, function () {
                console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
                console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
            });
        });
    };
    __decorate([
        blackbox_ioc_1.autowired('roles-service')
    ], AppInitialiser.prototype, "rolesService", void 0);
    __decorate([
        blackbox_ioc_1.autowired('users-service')
    ], AppInitialiser.prototype, "usersService", void 0);
    __decorate([
        blackbox_ioc_1.factory('rulebase')
    ], AppInitialiser.prototype, "createRuleBase", null);
    __decorate([
        blackbox_ioc_1.factory('oasDoc')
    ], AppInitialiser.prototype, "getOasDoc", null);
    __decorate([
        blackbox_ioc_1.taggedService('init', 'init-services')
    ], AppInitialiser.prototype, "init", null);
    return AppInitialiser;
}());
// Initialise all IOC services:
blackbox_ioc_1.callServices('init');
// Ref. http://expressjs.com/en/guide/error-handling.html
var clientErrorMiddleware = function (err, req, res, next) {
    if (typeof (err) === 'string') {
        // custom application error
        res.status(400).json({ error: "Error", message: err });
        return;
    }
    if (err.name === 'ClientError') {
        var ce = err;
        res.status(ce.code).json(ce);
        return;
    }
    // default to 500 server error
    // return res.status(500).json({ message: err.message });
    next(err);
};
