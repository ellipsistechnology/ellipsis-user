"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UsersPrivileges;
(function (UsersPrivileges) {
    UsersPrivileges["getUser"] = "Gets a user by name.";
    UsersPrivileges["getOptionsForUser"] = "Gets a users by name.";
    UsersPrivileges["replaceUser"] = "Replace the user identified by its name with the provided user.";
    UsersPrivileges["updateUser"] = "Change some of the fields of the user identified by its name.";
    UsersPrivileges["deleteUsers"] = "Delete the user identified by its name.";
    UsersPrivileges["getUsersList"] = "Retrieve a list of user objects.";
    UsersPrivileges["getOptionsForUsersService"] = "";
    UsersPrivileges["createUsers"] = "Creates a new user.";
})(UsersPrivileges || (UsersPrivileges = {}));
exports.default = UsersPrivileges;
