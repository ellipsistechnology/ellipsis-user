"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bad_Request = { code: 400, message: 'Bad Request' };
exports.Unauthorized = { code: 401, message: 'Unauthorized' };
exports.Payment_Required = { code: 402, message: 'Payment Required' };
exports.Forbidden = { code: 403, message: 'Forbidden' };
exports.Not_Found = { code: 404, message: 'Not Found' };
var ClientError = /** @class */ (function (_super) {
    __extends(ClientError, _super);
    function ClientError(cause, message) {
        var params = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            params[_i - 2] = arguments[_i];
        }
        var _this = _super.apply(this, params) || this;
        _this.code = cause.code;
        // Maintains proper stack trace for where our error was thrown (only available on V8)
        if (Error.captureStackTrace) {
            Error.captureStackTrace(_this, ClientError);
        }
        _this.name = 'ClientError';
        if (message)
            _this.message = message;
        else
            _this.message = cause.message;
        return _this;
    }
    return ClientError;
}(Error));
exports.default = ClientError;
