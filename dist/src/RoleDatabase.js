"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_database_1 = require("blackbox-database");
var blackbox_ioc_1 = require("blackbox-ioc");
var RoleDatabase = /** @class */ (function () {
    function RoleDatabase() {
    }
    RoleDatabase.prototype.getRole = function (_name) { return undefined; };
    RoleDatabase.prototype.getRoleList = function () { return undefined; };
    RoleDatabase.prototype.createRole = function (_role) { };
    RoleDatabase.prototype.updateRole = function (_name, _role) { };
    RoleDatabase.prototype.deleteRole = function (_name) { };
    RoleDatabase = __decorate([
        blackbox_database_1.database(),
        blackbox_ioc_1.named('role-database')
    ], RoleDatabase);
    return RoleDatabase;
}());
exports.default = RoleDatabase;
