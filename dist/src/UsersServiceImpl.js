"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var User_1 = __importDefault(require("./User"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var AuthenticationServiceImpl_1 = require("./AuthenticationServiceImpl");
var RolesServiceImpl_1 = require("./RolesServiceImpl");
var blackbox_client_1 = require("blackbox-client");
var ADMIN_USERNAME = 'admin';
var DEFAULT_ADMIN_PASSWORD = 'dotdotdot';
var ARCHIVE_SERVICE_URL = "http://localhost:8081/archive/data"; // TODO move to config
var USER_SERVICE_URL = "http://localhost:8088/users";
var UsersServiceImpl = /** @class */ (function () {
    function UsersServiceImpl() {
        if (!this.userdb)
            throw new Error("'user-database' service not found");
    }
    UsersServiceImpl.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            var adminUser, _a, _b, _c;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, this.userdb.getUser(ADMIN_USERNAME)];
                    case 1:
                        adminUser = _d.sent();
                        if (!!adminUser) return [3 /*break*/, 3];
                        _b = (_a = this.userdb).createUser;
                        _c = {
                            username: ADMIN_USERNAME
                        };
                        return [4 /*yield*/, bcrypt_1.default.hash(DEFAULT_ADMIN_PASSWORD, AuthenticationServiceImpl_1.SALT_ROUNDS)];
                    case 2:
                        _b.apply(_a, [(_c.password = _d.sent(),
                                _c.roles = [RolesServiceImpl_1.ADMIN_ROLE],
                                _c.authTypes = ['password'],
                                _c)]);
                        console.log("Created admin user - default password set to '" + DEFAULT_ADMIN_PASSWORD + "' - CHANGE PASSWORD BEFORE DEPLOYING TO PRODUCTION!");
                        _d.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UsersServiceImpl.prototype.getUsersList = function (_a) {
        var verbose = _a.verbose;
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.userdb.getUserList()];
                    case 1: return [2 /*return*/, (_b.sent())
                            .map(function (_a) {
                            var password = _a.password, userWithoutPassword = __rest(_a, ["password"]);
                            return (Object.assign(new User_1.default(), _this.makeUserVerbose(verbose, userWithoutPassword)));
                        })];
                }
            });
        });
    };
    UsersServiceImpl.prototype.getOptionsForUsersService = function (_props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'users');
    };
    UsersServiceImpl.prototype.hashPassword = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = user;
                        return [4 /*yield*/, bcrypt_1.default.hash(user.password, AuthenticationServiceImpl_1.SALT_ROUNDS)];
                    case 1:
                        _a.password = _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // TODO allow for multiple users
    UsersServiceImpl.prototype.createUsers = function (_a) {
        var user = _a.user;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.hashPassword(user)];
                    case 1:
                        _b.sent();
                        this.userdb.createUser(user);
                        return [2 /*return*/, { name: user.username }];
                }
            });
        });
    };
    UsersServiceImpl.prototype.makeUserVerbose = function (v, user) {
        if (v) {
            var roles = user.roles ?
                user.roles.reduce(function (map, role) {
                    map[role] = {
                        href: '/roles/' + role
                    };
                    return map;
                }, {})
                : {};
            roles.href = '/roles';
            roles.description = "The user's roles.";
            blackbox_services_1.verbose({ links: { roles: roles } })(user);
        }
        return user;
    };
    UsersServiceImpl.prototype.getUser = function (_a) {
        var name = _a.name, verbose = _a.verbose;
        return __awaiter(this, void 0, void 0, function () {
            var _b, password, userWithoutPassword, user;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, this.userdb.getUser(name)];
                    case 1:
                        _b = _c.sent(), password = _b.password, userWithoutPassword = __rest(_b, ["password"]);
                        user = Object.assign(new User_1.default(), userWithoutPassword);
                        return [2 /*return*/, this.makeUserVerbose(verbose, user)];
                }
            });
        });
    };
    UsersServiceImpl.prototype.getOptionsForUser = function (_props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'users');
    };
    UsersServiceImpl.prototype.replaceUser = function (_a) {
        var name = _a.name, user = _a.user;
        return __awaiter(this, void 0, void 0, function () {
            var oldUser;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!!user.password) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.userdb.getUser(name)];
                    case 1:
                        oldUser = _b.sent();
                        user.password = oldUser.password;
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.hashPassword(user)];
                    case 3:
                        _b.sent();
                        _b.label = 4;
                    case 4:
                        user.privileges = undefined;
                        delete user._id;
                        return [4 /*yield*/, this.userdb.updateUser(name, user)];
                    case 5:
                        _b.sent();
                        return [2 /*return*/, { name: user.username }];
                }
            });
        });
    };
    UsersServiceImpl.prototype.updateUser = function (_a) {
        var name = _a.name, user = _a.user;
        return __awaiter(this, void 0, void 0, function () {
            var oldUser, newUser;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.userdb.getUser(name)];
                    case 1:
                        oldUser = _b.sent();
                        newUser = Object.assign(oldUser, user);
                        delete newUser._id;
                        newUser.privileges = undefined;
                        return [4 /*yield*/, this.hashPassword(newUser)];
                    case 2:
                        _b.sent();
                        return [4 /*yield*/, this.userdb.updateUser(name, newUser)];
                    case 3:
                        _b.sent();
                        return [2 /*return*/, { name: newUser.username }];
                }
            });
        });
    };
    UsersServiceImpl.prototype.deleteUser = function (_a) {
        var name = _a.name;
        return __awaiter(this, void 0, void 0, function () {
            var user, id;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.userdb.getUser(name)];
                    case 1:
                        user = _b.sent();
                        delete user._id;
                        return [4 /*yield*/, new blackbox_client_1.DataClient(ARCHIVE_SERVICE_URL).post({
                                data: user,
                                type: 'delete',
                                serviceUrl: USER_SERVICE_URL
                            })];
                    case 2:
                        id = (_b.sent()).name;
                        return [4 /*yield*/, this.userdb.deleteUser(name)];
                    case 3:
                        _b.sent();
                        return [2 /*return*/, { name: name, archiveId: id }];
                }
            });
        });
    };
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], UsersServiceImpl.prototype, "oasDoc", void 0);
    __decorate([
        blackbox_ioc_1.autowired('user-database')
    ], UsersServiceImpl.prototype, "userdb", void 0);
    __decorate([
        blackbox_ioc_1.taggedService('init', 'init-users-service')
    ], UsersServiceImpl.prototype, "init", null);
    UsersServiceImpl = __decorate([
        blackbox_ioc_1.serviceClass('users-service')
    ], UsersServiceImpl);
    return UsersServiceImpl;
}());
exports.UsersServiceImpl = UsersServiceImpl;
