"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var SecurityServiceImpl = /** @class */ (function () {
    function SecurityServiceImpl() {
    }
    SecurityServiceImpl.prototype.getSecurity = function (props) {
        throw new Error("getSecurity not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.getRoles = function (props) {
        throw new Error("getRoles not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.createRole = function (props) {
        throw new Error("createRole not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.getRole = function (props) {
        throw new Error("getRole not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.replaceRole = function (props) {
        throw new Error("replaceRole not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.updateRole = function (props) {
        throw new Error("updateRole not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.deleteRole = function (props) {
        throw new Error("deleteRole not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.getPrivileges = function (props) {
        throw new Error("getPrivileges not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.createPrivilege = function (props) {
        throw new Error("createPrivilege not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.getPrivilege = function (props) {
        throw new Error("getPrivilege not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.replacePrivilege = function (props) {
        throw new Error("replacePrivilege not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.updatePrivilege = function (props) {
        throw new Error("updatePrivilege not yet implemented. Called with props=" + JSON.stringify(props));
    };
    SecurityServiceImpl.prototype.deletePrivilege = function (props) {
        throw new Error("deletePrivilege not yet implemented. Called with props=" + JSON.stringify(props));
    };
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], SecurityServiceImpl.prototype, "oasDoc", void 0);
    SecurityServiceImpl = __decorate([
        blackbox_ioc_1.serviceClass('security-service')
    ], SecurityServiceImpl);
    return SecurityServiceImpl;
}());
exports.SecurityServiceImpl = SecurityServiceImpl;
