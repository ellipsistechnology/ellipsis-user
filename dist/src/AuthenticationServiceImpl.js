"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var fs_1 = __importDefault(require("fs"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var blackbox_services_1 = require("blackbox-services");
exports.SALT_ROUNDS = 10;
var OAuth2Client = require('google-auth-library').OAuth2Client;
var credentials_json_1 = __importDefault(require("./credentials.json"));
var ClientError_1 = __importStar(require("./ClientError"));
var AuthenticationServiceImpl = /** @class */ (function () {
    function AuthenticationServiceImpl() {
        this.googelClient = new OAuth2Client(credentials_json_1.default.web.client_id);
    }
    AuthenticationServiceImpl.prototype.getAuthenticationList = function (props) {
        var publicKey = fs_1.default.readFileSync('key.pub').toString();
        if (publicKey.endsWith('\n'))
            publicKey = publicKey.substring(0, publicKey.length - 1);
        return [{
                type: "public-key",
                key: publicKey.toString()
            }];
    };
    AuthenticationServiceImpl.prototype.authenticateByPassword = function (authentication) {
        return __awaiter(this, void 0, void 0, function () {
            var user, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!authentication.user || !authentication.user.username || !authentication.user.password)
                            throw new ClientError_1.default(ClientError_1.Unauthorized, "user object with username and password must be provided when authenticating with password.");
                        return [4 /*yield*/, this.userdb.getUser(authentication.user.username)];
                    case 1:
                        user = _b.sent();
                        _a = !user ||
                            !Array.isArray(user.authTypes) ||
                            !user.authTypes.includes('password') ||
                            !user.password;
                        if (_a) return [3 /*break*/, 3];
                        return [4 /*yield*/, bcrypt_1.default.compare(authentication.user.password, user.password)];
                    case 2:
                        _a = !(_b.sent());
                        _b.label = 3;
                    case 3:
                        if (_a) {
                            throw new ClientError_1.default(ClientError_1.Unauthorized, "Invalid username or password");
                        }
                        return [2 /*return*/, user];
                }
            });
        });
    };
    AuthenticationServiceImpl.prototype.authenticateByGoogle = function (authentication) {
        return __awaiter(this, void 0, void 0, function () {
            var user, ticket, payload, username, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!authentication.token || !authentication.user)
                            throw new ClientError_1.default(ClientError_1.Unauthorized, "token and user must be provided when authenticating with Google.");
                        return [4 /*yield*/, this.userdb.getUser(authentication.user.username)];
                    case 1:
                        user = _a.sent();
                        if (!user || !Array.isArray(user.authTypes) || !user.authTypes.includes('google'))
                            throw new ClientError_1.default(ClientError_1.Unauthorized, "Invalid token for user.");
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.googelClient.verifyIdToken({
                                idToken: authentication.token,
                                audience: credentials_json_1.default.web.client_id
                            })];
                    case 3:
                        ticket = _a.sent();
                        payload = ticket.getPayload();
                        username = payload['sub'];
                        if (username !== user.username)
                            throw new Error("Invalid token for user.");
                        // TODO lazily setup user and profile (profile may be another microservice, e.g. {
                        //   user: User or string for username
                        //   person: Person
                        //   profile details specific to the site using the service
                        // }
                        return [2 /*return*/, user];
                    case 4:
                        err_1 = _a.sent();
                        throw new ClientError_1.default(ClientError_1.Unauthorized, err_1.toString());
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    AuthenticationServiceImpl.prototype.createAuthentication = function (_a) {
        var authentication = _a.authentication;
        return __awaiter(this, void 0, void 0, function () {
            var user, _b, privateKey, payload, token;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = authentication.type;
                        switch (_b) {
                            case 'password': return [3 /*break*/, 1];
                            case 'google': return [3 /*break*/, 3];
                        }
                        return [3 /*break*/, 5];
                    case 1: return [4 /*yield*/, this.authenticateByPassword(authentication)];
                    case 2:
                        user = _c.sent();
                        return [3 /*break*/, 6];
                    case 3: return [4 /*yield*/, this.authenticateByGoogle(authentication)];
                    case 4:
                        user = _c.sent();
                        return [3 /*break*/, 6];
                    case 5: throw new ClientError_1.default(ClientError_1.Unauthorized, "Invalid authentication type: type: " + authentication.type);
                    case 6:
                        privateKey = fs_1.default.readFileSync('key');
                        payload = {
                            sub: user.username,
                            priv: user.privileges,
                            iss: "bb",
                        };
                        token = jsonwebtoken_1.default.sign(payload, privateKey, { algorithm: 'RS256' });
                        return [2 /*return*/, {
                                type: 'jwt',
                                name: user.username.toString(),
                                token: token
                            }];
                }
            });
        });
    };
    AuthenticationServiceImpl.prototype.getOptionsForAuthenticationService = function (props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'authentication');
    };
    AuthenticationServiceImpl.prototype.getAuthentication = function (props) {
        throw new Error("getAuthentication not yet implemented. Called with props=" + JSON.stringify(props));
    };
    AuthenticationServiceImpl.prototype.getOptionsForAuthentication = function (props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'authentication');
    };
    AuthenticationServiceImpl.prototype.replaceAuthentication = function (props) {
        throw new Error("replaceAuthentication not yet implemented. Called with props=" + JSON.stringify(props));
    };
    AuthenticationServiceImpl.prototype.updateAuthentication = function (props) {
        throw new Error("updateAuthentication not yet implemented. Called with props=" + JSON.stringify(props));
    };
    AuthenticationServiceImpl.prototype.deleteAuthentication = function (props) {
        throw new Error("deleteAuthentication not yet implemented. Called with props=" + JSON.stringify(props));
    };
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], AuthenticationServiceImpl.prototype, "oasDoc", void 0);
    __decorate([
        blackbox_ioc_1.autowired('user-database')
    ], AuthenticationServiceImpl.prototype, "userdb", void 0);
    AuthenticationServiceImpl = __decorate([
        blackbox_ioc_1.serviceClass('authentication-service')
    ], AuthenticationServiceImpl);
    return AuthenticationServiceImpl;
}());
exports.AuthenticationServiceImpl = AuthenticationServiceImpl;
