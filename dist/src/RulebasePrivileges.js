"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RulebasePrivileges;
(function (RulebasePrivileges) {
    RulebasePrivileges["getRules"] = "Provides a list of black box rules.";
    RulebasePrivileges["createRule"] = "Creates a rule.";
    RulebasePrivileges["getConditions"] = "Provides a list of black box conditions.";
    RulebasePrivileges["createCondition"] = "Creates a condition.";
    RulebasePrivileges["getValues"] = "Provides a list of black box values.";
    RulebasePrivileges["createValue"] = "Creates a value.";
    RulebasePrivileges["getRulebaseService"] = "The blackbox rulebase service.";
})(RulebasePrivileges || (RulebasePrivileges = {}));
exports.default = RulebasePrivileges;
