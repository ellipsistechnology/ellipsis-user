"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ClientError_1 = __importStar(require("./ClientError"));
var jsonwebtoken_1 = require("jsonwebtoken");
var fs_1 = __importDefault(require("fs"));
exports.authenticationMiddleware = function (_a) {
    var ignore = (_a === void 0 ? {} : _a).ignore;
    return function (req, res, next) {
        if (ignore.includes(req.path))
            return next();
        if (req.headers.authorization) {
            var token = req.headers.authorization.match(/(?<=Bearer\s)[^\s]*/);
            if (token) {
                try {
                    var publicKey = fs_1.default.readFileSync('key.pub').toString();
                    var _a = jsonwebtoken_1.verify(token.toString(), publicKey, { algorithms: ['RS256'], ignoreExpiration: true }), priv = _a.priv, sub = _a.sub; // TODO don't ignore expiration
                    if (!Array.isArray(priv) || (typeof sub !== 'string') || sub.length < 1)
                        return next(new ClientError_1.default(ClientError_1.Unauthorized, 'Invalid token.'));
                    req.user = {
                        username: sub,
                        privileges: priv
                    };
                    return next();
                }
                catch (err) {
                    console.error(err);
                    return next(new ClientError_1.default(ClientError_1.Unauthorized, 'Invalid token.'));
                }
            }
            else {
                return next(new ClientError_1.default(ClientError_1.Forbidden, 'Authorization required.'));
            }
        }
        else {
            return next(new ClientError_1.default(ClientError_1.Forbidden, 'Authorization required.'));
        }
    };
};
exports.authorisationMiddleware = function (_a) {
    var ignore = (_a === void 0 ? {} : _a).ignore;
    return function (req, res, next) {
        if (ignore.includes(req.path))
            return next();
        try {
            if (!req.swagger || !req.swagger.operation) // let the router handle the lack of support for this method
                return next();
            var operationId = req.swagger.operation.operationId;
            if (!req.user.privileges.includes(operationId))
                return next(new ClientError_1.default(ClientError_1.Unauthorized, "User " + req.user.username + " does not have required permission: " + operationId + "."));
        }
        catch (err) {
            return next(err);
        }
        next();
    };
};
