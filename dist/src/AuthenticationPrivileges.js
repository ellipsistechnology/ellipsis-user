"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AuthenticationPrivileges;
(function (AuthenticationPrivileges) {
    AuthenticationPrivileges["getAuthenticationList"] = "Retrieve a list of authentication objects.";
    AuthenticationPrivileges["getOptionsForAuthenticationService"] = "";
    AuthenticationPrivileges["createAuthentication"] = "Creates a new authentication of type authentication.";
})(AuthenticationPrivileges || (AuthenticationPrivileges = {}));
exports.default = AuthenticationPrivileges;
