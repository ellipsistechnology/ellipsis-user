"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var User_1 = __importDefault(require("./User"));
var blackbox_database_1 = require("blackbox-database");
var blackbox_database_2 = require("blackbox-database");
var USER_COLLECTION = 'user';
var ROLE_COLLECTION = 'role';
function ensurePrivileges(db, _id, user) {
    return __awaiter(this, void 0, void 0, function () {
        var privs, _i, _a, roleName, role;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!(user && !Array.isArray(user.privileges))) return [3 /*break*/, 8];
                    if (!Array.isArray(user.roles)) return [3 /*break*/, 5];
                    privs = [];
                    _i = 0, _a = user.roles;
                    _b.label = 1;
                case 1:
                    if (!(_i < _a.length)) return [3 /*break*/, 4];
                    roleName = _a[_i];
                    return [4 /*yield*/, db.collection(ROLE_COLLECTION).findOne({ name: roleName })];
                case 2:
                    role = _b.sent();
                    if (role)
                        privs = privs.concat(role.privileges);
                    _b.label = 3;
                case 3:
                    _i++;
                    return [3 /*break*/, 1];
                case 4:
                    user.privileges = privs;
                    return [3 /*break*/, 6];
                case 5:
                    user.privileges = [];
                    _b.label = 6;
                case 6: return [4 /*yield*/, db.collection(USER_COLLECTION).replaceOne({ username: user.username }, user)];
                case 7:
                    _b.sent();
                    _b.label = 8;
                case 8: return [2 /*return*/, user];
            }
        });
    });
}
var UserDatabase = /** @class */ (function () {
    function UserDatabase() {
    }
    UserDatabase.prototype.getUserList = function () {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, {}];
        }); });
    };
    UserDatabase.prototype.getUser = function (_username) {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/, undefined];
        }); });
    };
    UserDatabase.prototype.createUser = function (_user) {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); });
    };
    UserDatabase.prototype.updateUser = function (_username, _user) {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); });
    };
    UserDatabase.prototype.deleteUser = function (_username) {
        return __awaiter(this, void 0, void 0, function () { return __generator(this, function (_a) {
            return [2 /*return*/];
        }); });
    };
    __decorate([
        blackbox_database_1.dbGetList('User', function (db, users) { return __awaiter(_this, void 0, void 0, function () {
            var _i, users_1, user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _i = 0, users_1 = users;
                        _a.label = 1;
                    case 1:
                        if (!(_i < users_1.length)) return [3 /*break*/, 4];
                        user = users_1[_i];
                        return [4 /*yield*/, ensurePrivileges(db, user.username, user)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, users.map(function (user) { return Object.assign(new User_1.default(), user); })];
                }
            });
        }); })
    ], UserDatabase.prototype, "getUserList", null);
    __decorate([
        blackbox_database_1.dbGet('User', 'username', function (db, username, user) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, ensurePrivileges(db, username, user)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, user];
                }
            });
        }); })
    ], UserDatabase.prototype, "getUser", null);
    __decorate([
        blackbox_database_1.dbCreate('User')
    ], UserDatabase.prototype, "createUser", null);
    __decorate([
        blackbox_database_1.dbUpdate('User', 'username')
    ], UserDatabase.prototype, "updateUser", null);
    __decorate([
        blackbox_database_2.dbDelete('User', 'username')
    ], UserDatabase.prototype, "deleteUser", null);
    UserDatabase = __decorate([
        blackbox_ioc_1.named('user-database')
    ], UserDatabase);
    return UserDatabase;
}());
exports.default = UserDatabase;
