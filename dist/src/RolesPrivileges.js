"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RolesPrivileges;
(function (RolesPrivileges) {
    RolesPrivileges["getRole"] = "Gets a role by name.";
    RolesPrivileges["getOptionsForRole"] = "Gets a role by name.";
    RolesPrivileges["replaceRoles"] = "Replace the role identified by its name.";
    RolesPrivileges["updateRole"] = "Change some of the fields of the role identified by its name.";
    RolesPrivileges["deleteRole"] = "Delete the role identified by its name.";
    RolesPrivileges["getRolesList"] = "Retrieve a list of role objects.";
    RolesPrivileges["getOptionsForRolesService"] = "";
    RolesPrivileges["createRoles"] = "Creates a new role.";
})(RolesPrivileges || (RolesPrivileges = {}));
exports.default = RolesPrivileges;
