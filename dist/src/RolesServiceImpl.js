"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var Role_1 = __importDefault(require("./Role"));
var UsersPrivileges_1 = __importDefault(require("./UsersPrivileges"));
var RulebasePrivileges_1 = __importDefault(require("./RulebasePrivileges")); // TODO move to rules-service
var RolesPrivileges_1 = __importDefault(require("./RolesPrivileges"));
exports.ADMIN_ROLE = 'Administrator';
var RolesServiceImpl = /** @class */ (function () {
    function RolesServiceImpl() {
    }
    RolesServiceImpl.prototype.init = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.rolesDb.getRole(exports.ADMIN_ROLE)];
                    case 1:
                        // Ensure the admin role exists with all user privileges:
                        if (!(_a.sent())) {
                            this.rolesDb.createRole({
                                name: exports.ADMIN_ROLE,
                                privileges: Object.keys(UsersPrivileges_1.default)
                                    .concat(Object.keys(RulebasePrivileges_1.default))
                                    .concat(Object.keys(RolesPrivileges_1.default))
                            });
                            console.log("Created Administrator role.");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    RolesServiceImpl.prototype.getRolesList = function (_props) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.rolesDb.getRoleList()];
                    case 1: return [2 /*return*/, (_a.sent()).map(function (role) { return Object.assign(new Role_1.default(), role); })];
                }
            });
        });
    };
    RolesServiceImpl.prototype.getOptionsForRolesService = function (_props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'roles');
    };
    RolesServiceImpl.prototype.createRoles = function (_a) {
        var role = _a.role;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.rolesDb.createRole(role)];
                    case 1:
                        _b.sent();
                        return [2 /*return*/, { name: role.name }];
                }
            });
        });
    };
    RolesServiceImpl.prototype.getRole = function (_a) {
        var name = _a.name;
        return __awaiter(this, void 0, void 0, function () {
            var _b, _c, _d;
            return __generator(this, function (_e) {
                switch (_e.label) {
                    case 0:
                        _c = (_b = Object).assign;
                        _d = [new Role_1.default()];
                        return [4 /*yield*/, this.rolesDb.getRole(name)];
                    case 1: return [2 /*return*/, _c.apply(_b, _d.concat([_e.sent()]))];
                }
            });
        });
    };
    RolesServiceImpl.prototype.getOptionsForRole = function (_props) {
        return blackbox_services_1.makeServiceObject(this.oasDoc, 'roles');
    };
    // FIXME need to invalidate users so privileges are reloaded
    RolesServiceImpl.prototype.replaceRole = function (_a) {
        var name = _a.name, role = _a.role;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        delete role._id;
                        return [4 /*yield*/, this.rolesDb.updateRole(name, role)];
                    case 1:
                        _b.sent();
                        return [2 /*return*/, { name: name }];
                }
            });
        });
    };
    // FIXME need to invalidate users so privileges are reloaded
    RolesServiceImpl.prototype.updateRole = function (_a) {
        var name = _a.name, role = _a.role;
        return __awaiter(this, void 0, void 0, function () {
            var oldRole, newRole;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.rolesDb.getRole(name)];
                    case 1:
                        oldRole = _b.sent();
                        newRole = Object.assign(oldRole, role);
                        delete newRole._id;
                        return [4 /*yield*/, this.rolesDb.updateRole(name, newRole)];
                    case 2:
                        _b.sent();
                        return [2 /*return*/, { name: name }];
                }
            });
        });
    };
    RolesServiceImpl.prototype.deleteRole = function (_a) {
        var name = _a.name;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.rolesDb.deleteRole(name)];
                    case 1:
                        _b.sent();
                        return [2 /*return*/, { name: name }];
                }
            });
        });
    };
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], RolesServiceImpl.prototype, "oasDoc", void 0);
    __decorate([
        blackbox_ioc_1.autowired('role-database')
    ], RolesServiceImpl.prototype, "rolesDb", void 0);
    __decorate([
        blackbox_ioc_1.taggedService('init', 'init-roles-service')
    ], RolesServiceImpl.prototype, "init", null);
    RolesServiceImpl = __decorate([
        blackbox_ioc_1.serviceClass('roles-service')
    ], RolesServiceImpl);
    return RolesServiceImpl;
}());
exports.RolesServiceImpl = RolesServiceImpl;
