'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var RulebaseServiceWrapper = /** @class */ (function () {
    function RulebaseServiceWrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('rulebase-service')
    ], RulebaseServiceWrapper.prototype, "service", void 0);
    return RulebaseServiceWrapper;
}());
var rulebaseWrapper = new RulebaseServiceWrapper();
function strip__(target) {
    if (Array.isArray(target)) {
        return target.map(strip__);
    }
    else {
        var ret_1 = {};
        Object.keys(target).forEach(function (key) {
            if (!key.startsWith('__'))
                ret_1[key] = target[key];
        });
        return ret_1;
    }
}
function convertBooleanParameter(val) {
    if (val === 'false')
        return false;
    if (val === '')
        return true;
    return val;
}
/**
 * Creates a condition.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * condition Condition A blackbox condition. (optional)
 * returns named-reference
 **/
exports.createCondition = function (verbose, condition) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.createCondition({
                            verbose: verbose,
                            condition: condition
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Creates a rule.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * rule Rule A blackbox rule. (optional)
 * returns named-reference
 **/
exports.createRule = function (verbose, rule) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.createRule({
                            verbose: verbose,
                            rule: rule
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Creates a value.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * value Value A blackbox value. (optional)
 * returns named-reference
 **/
exports.createValue = function (verbose, value) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.createValue({
                            verbose: verbose,
                            value: value
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Delete the condition identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteCondition = function (name) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.deleteCondition({
                        name: name
                    }))];
                case 1:
                    val = _a.sent();
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = {
                        "name" : "name"
                      };
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Delete the rule identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteRule = function (name) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.deleteRule({
                        name: name
                    }))];
                case 1:
                    val = _a.sent();
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = {
                        "name" : "name"
                      };
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Delete the value identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteValue = function (name) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.deleteValue({
                        name: name
                    }))];
                case 1:
                    val = _a.sent();
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = {
                        "name" : "name"
                      };
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Request a condition identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns condition
 **/
exports.getCondition = function (name, verbose, depth) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.getCondition({
                            name: name,
                            verbose: verbose,
                            depth: depth
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Provides a list of black box conditions.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns List
 **/
exports.getConditions = function (verbose, depth) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.getConditions({
                            verbose: verbose,
                            depth: depth
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = "";
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Request a rule identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns rule
 **/
exports.getRule = function (name, verbose, depth) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.getRule({
                            name: name,
                            verbose: verbose,
                            depth: depth
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * The blackbox rulebase service.
 *
 * returns service
 **/
exports.getRulebaseService = function () {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.getRulebaseService({}))];
                case 1:
                    val = _a.sent();
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = {
                        "bbversion" : "bbversion",
                        "description" : "description",
                        "links" : {
                          "key" : ""
                        }
                      };
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Provides a list of black box rules.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns List
 **/
exports.getRules = function (verbose, depth) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.getRules({
                            verbose: verbose,
                            depth: depth
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = "";
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Request a value identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns value
 **/
exports.getValue = function (name, verbose, depth) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.getValue({
                            name: name,
                            verbose: verbose,
                            depth: depth
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Provides a list of black box values.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns List
 **/
exports.getValues = function (verbose, depth) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.getValues({
                            verbose: verbose,
                            depth: depth
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = "";
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Replace the condition identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * condition Condition A Blackbox condition. (optional)
 * returns named-reference
 **/
exports.replaceCondition = function (name, verbose, condition) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.replaceCondition({
                            name: name,
                            verbose: verbose,
                            condition: condition
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Replace the rule identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * rule Rule A Blackbox rule. (optional)
 * returns named-reference
 **/
exports.replaceRule = function (name, verbose, rule) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.replaceRule({
                            name: name,
                            verbose: verbose,
                            rule: rule
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Replace the value identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * value Value A Blackbox value. (optional)
 * returns named-reference
 **/
exports.replaceValue = function (name, verbose, value) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.replaceValue({
                            name: name,
                            verbose: verbose,
                            value: value
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Change some of the fields of the condition identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * condition Condition A Blackbox condition. (optional)
 * returns named-reference
 **/
exports.updateCondition = function (name, verbose, condition) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.updateCondition({
                            name: name,
                            verbose: verbose,
                            condition: condition
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Change some of the fields of the rule identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * rule Rule A Blackbox rule. (optional)
 * returns named-reference
 **/
exports.updateRule = function (name, verbose, rule) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.updateRule({
                            name: name,
                            verbose: verbose,
                            rule: rule
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Change some of the fields of the value identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * value Value A Blackbox value. (optional)
 * returns named-reference
 **/
exports.updateValue = function (name, verbose, value) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rulebaseWrapper.service.updateValue({
                            name: name,
                            verbose: verbose,
                            value: value
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
