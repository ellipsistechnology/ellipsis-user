'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var RolesServiceWrapper = /** @class */ (function () {
    function RolesServiceWrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('roles-service')
    ], RolesServiceWrapper.prototype, "service", void 0);
    return RolesServiceWrapper;
}());
var rolesWrapper = new RolesServiceWrapper();
function strip__(target) {
    if (Array.isArray(target)) {
        return target.map(strip__);
    }
    else {
        var ret_1 = {};
        Object.keys(target).forEach(function (key) {
            if (!key.startsWith('__'))
                ret_1[key] = target[key];
        });
        return ret_1;
    }
}
function convertBooleanParameter(val) {
    if (val === 'false')
        return false;
    if (val === '')
        return true;
    return val;
}
/**
 * Creates a new role.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.createRoles = function (verbose, role) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rolesWrapper.service.createRoles({
                            verbose: verbose,
                            role: role
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Delete the role identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteRole = function (name) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Promise.resolve(rolesWrapper.service.deleteRole({
                        name: name
                    }))];
                case 1:
                    val = _a.sent();
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = {
                        "name" : "name"
                      };
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Gets a role by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns service
 **/
exports.getOptionsForRole = function (name, verbose, depth) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rolesWrapper.service.getOptionsForRole({
                            name: name,
                            verbose: verbose,
                            depth: depth
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = {
                        "bbversion" : "bbversion",
                        "description" : "description",
                        "links" : {
                          "key" : ""
                        }
                      };
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns service
 **/
exports.getOptionsForRolesService = function (depth, verbose) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rolesWrapper.service.getOptionsForRolesService({
                            depth: depth,
                            verbose: verbose
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = {
                        "bbversion" : "bbversion",
                        "description" : "description",
                        "links" : {
                          "key" : ""
                        }
                      };
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Gets a role by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns role
 **/
exports.getRole = function (name, verbose, depth) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rolesWrapper.service.getRole({
                            name: name,
                            verbose: verbose,
                            depth: depth
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Retrieve a list of role objects.
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns List
 **/
exports.getRolesList = function (depth, verbose) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rolesWrapper.service.getRolesList({
                            depth: depth,
                            verbose: verbose
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    val = blackbox_services_1.trimToDepth(val, depth);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        examples['application/json'] = "";
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Replace the role identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.replaceRoles = function (name, verbose, role) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rolesWrapper.service.replaceRole({
                            name: name,
                            verbose: verbose,
                            role: role
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
/**
 * Change some of the fields of the role identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.updateRole = function (name, verbose, role) {
    return __awaiter(this, void 0, void 0, function () {
        var val;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    verbose = convertBooleanParameter(verbose);
                    return [4 /*yield*/, Promise.resolve(rolesWrapper.service.updateRole({
                            name: name,
                            verbose: verbose,
                            role: role
                        }))];
                case 1:
                    val = _a.sent();
                    val = blackbox_services_1.verboseResponse(val, verbose);
                    return [2 /*return*/, strip__(val)
                        // Return value:
                        /* TODO: Allow for examples when using OPTIONS or
                        var examples = {};
                        if (Object.keys(examples).length > 0) {
                          resolve(examples[Object.keys(examples)[0]]);
                        } else {
                          resolve();
                        }
                        */
                    ];
            }
        });
    });
};
