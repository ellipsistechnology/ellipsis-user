'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var SecurityServiceWrapper = /** @class */ (function () {
    function SecurityServiceWrapper() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('security-service')
    ], SecurityServiceWrapper.prototype, "service", void 0);
    return SecurityServiceWrapper;
}());
var securityWrapper = new SecurityServiceWrapper();
/**
 * Creates a new privilege of type privilege.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * privilege Privilege A privilege. (optional)
 * returns named-reference
 **/
exports.createPrivilege = function (verbose, privilege) {
    return new Promise(function (resolve, reject) {
        // Call service provider for createPrivilege:
        try {
            var val = securityWrapper.service.createPrivilege({
                verbose: verbose,
                privilege: privilege
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Creates a new role of type role.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.createRole = function (verbose, role) {
    return new Promise(function (resolve, reject) {
        // Call service provider for createRole:
        try {
            var val = securityWrapper.service.createRole({
                verbose: verbose,
                role: role
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Delete the privilege identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deletePrivilege = function (name) {
    return new Promise(function (resolve, reject) {
        // Call service provider for deletePrivilege:
        try {
            var val = securityWrapper.service.deletePrivilege({
                name: name
            });
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        examples['application/json'] = {
      "name" : "name"
    };
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Delete the role identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteRole = function (name) {
    return new Promise(function (resolve, reject) {
        // Call service provider for deleteRole:
        try {
            var val = securityWrapper.service.deleteRole({
                name: name
            });
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        examples['application/json'] = {
      "name" : "name"
    };
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Gets a privilege by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns privilege
 **/
exports.getPrivilege = function (name, verbose, depth) {
    return new Promise(function (resolve, reject) {
        // Call service provider for getPrivilege:
        try {
            var val = securityWrapper.service.getPrivilege({
                name: name,
                verbose: verbose,
                depth: depth
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Gets a privilege by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns privilege
 **/
exports.getPrivilege_0 = function (name, verbose, depth) {
    return new Promise(function (resolve, reject) {
        // Call service provider for getPrivilege_0:
        try {
            var val = securityWrapper.service.getPrivilege_0({
                name: name,
                verbose: verbose,
                depth: depth
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Retrieve a list of privilege objects.
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns List
 **/
exports.getPrivileges = function (depth, verbose) {
    return new Promise(function (resolve, reject) {
        // Call service provider for getPrivileges:
        try {
            var val = securityWrapper.service.getPrivileges({
                depth: depth,
                verbose: verbose
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        examples['application/json'] = "";
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Gets a role by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns role
 **/
exports.getRole = function (name, verbose, depth) {
    return new Promise(function (resolve, reject) {
        // Call service provider for getRole:
        try {
            var val = securityWrapper.service.getRole({
                name: name,
                verbose: verbose,
                depth: depth
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Gets a role by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns role
 **/
exports.getRole_0 = function (name, verbose, depth) {
    return new Promise(function (resolve, reject) {
        // Call service provider for getRole_0:
        try {
            var val = securityWrapper.service.getRole_0({
                name: name,
                verbose: verbose,
                depth: depth
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Retrieve a list of role objects.
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns List
 **/
exports.getRoles = function (depth, verbose) {
    return new Promise(function (resolve, reject) {
        // Call service provider for getRoles:
        try {
            var val = securityWrapper.service.getRoles({
                depth: depth,
                verbose: verbose
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        examples['application/json'] = "";
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns service
 **/
exports.getSecurity = function (depth, verbose) {
    return new Promise(function (resolve, reject) {
        // Call service provider for getSecurity:
        try {
            var val = securityWrapper.service.getSecurity({
                depth: depth,
                verbose: verbose
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            val = blackbox_services_1.trimToDepth(val, depth);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        examples['application/json'] = {
      "bbversion" : "bbversion",
      "description" : "description",
      "links" : {
        "key" : ""
      }
    };
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Replace the privilege identified by its name with the provided privilege.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * privilege Privilege A privilege. (optional)
 * returns named-reference
 **/
exports.replacePrivilege = function (name, verbose, privilege) {
    return new Promise(function (resolve, reject) {
        // Call service provider for replacePrivilege:
        try {
            var val = securityWrapper.service.replacePrivilege({
                name: name,
                verbose: verbose,
                privilege: privilege
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Replace the role identified by its name with the provided role.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.replaceRole = function (name, verbose, role) {
    return new Promise(function (resolve, reject) {
        // Call service provider for replaceRole:
        try {
            var val = securityWrapper.service.replaceRole({
                name: name,
                verbose: verbose,
                role: role
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Change some of the fields of the privilege identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * privilege Privilege A privilege. (optional)
 * returns named-reference
 **/
exports.updatePrivilege = function (name, verbose, privilege) {
    return new Promise(function (resolve, reject) {
        // Call service provider for updatePrivilege:
        try {
            var val = securityWrapper.service.updatePrivilege({
                name: name,
                verbose: verbose,
                privilege: privilege
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
/**
 * Change some of the fields of the role identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.updateRole = function (name, verbose, role) {
    return new Promise(function (resolve, reject) {
        // Call service provider for updateRole:
        try {
            var val = securityWrapper.service.updateRole({
                name: name,
                verbose: verbose,
                role: role
            });
            val = blackbox_services_1.verboseResponse(val, verbose);
            resolve(val);
        }
        catch (err) {
            console.error(err);
            if (typeof err === 'string')
                reject(err);
            if (err instanceof Error)
                reject({
                    error: {
                        name: err.name,
                        description: err.message
                    }
                });
            else
                reject({ error: err });
        }
        // Return value:
        /* TODO: Allow for examples when using OPTIONS or
        var examples = {};
        if (Object.keys(examples).length > 0) {
          resolve(examples[Object.keys(examples)[0]]);
        } else {
          resolve();
        }
        */
    });
};
