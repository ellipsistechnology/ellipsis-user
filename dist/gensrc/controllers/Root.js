'use strict';
var utils = require('../utils/writer.js');
var Root = require('../service/RootService');
module.exports.getRootService = function getRootService(req, res, next) {
    Root.getRootService()
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
