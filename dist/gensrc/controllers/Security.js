'use strict';
var utils = require('../utils/writer.js');
var Security = require('../service/SecurityService');
module.exports.createPrivilege = function createPrivilege(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var privilege = req.body;
    Security.createPrivilege(verbose, privilege)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.createRole = function createRole(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var role = req.body;
    Security.createRole(verbose, role)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.deletePrivilege = function deletePrivilege(req, res, next) {
    var name = req.swagger.params['name'].value;
    Security.deletePrivilege(name)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.deleteRole = function deleteRole(req, res, next) {
    var name = req.swagger.params['name'].value;
    Security.deleteRole(name)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.getPrivilege = function getPrivilege(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Security.getPrivilege(name, verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.getPrivilege_1 = function getPrivilege_1(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Security.getPrivilege_1(name, verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.getPrivileges = function getPrivileges(req, res, next) {
    var depth = req.swagger.params['depth'].value;
    var verbose = req.swagger.params['verbose'].value;
    Security.getPrivileges(depth, verbose)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.getRole = function getRole(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Security.getRole(name, verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.getRole_2 = function getRole_2(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Security.getRole_2(name, verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.getRoles = function getRoles(req, res, next) {
    var depth = req.swagger.params['depth'].value;
    var verbose = req.swagger.params['verbose'].value;
    Security.getRoles(depth, verbose)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.getSecurity = function getSecurity(req, res, next) {
    var depth = req.swagger.params['depth'].value;
    var verbose = req.swagger.params['verbose'].value;
    Security.getSecurity(depth, verbose)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.replacePrivilege = function replacePrivilege(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var privilege = req.body;
    Security.replacePrivilege(name, verbose, privilege)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.replaceRole = function replaceRole(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var role = req.body;
    Security.replaceRole(name, verbose, role)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.updatePrivilege = function updatePrivilege(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var privilege = req.body;
    Security.updatePrivilege(name, verbose, privilege)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
module.exports.updateRole = function updateRole(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var role = req.body;
    Security.updateRole(name, verbose, role)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        utils.writeJson(res, response);
    });
};
