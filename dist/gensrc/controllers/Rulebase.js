'use strict';
var utils = require('../utils/writer.js');
var Rulebase = require('../service/RulebaseService');
module.exports.createCondition = function createCondition(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var condition = req.body;
    Rulebase.createCondition(verbose, condition)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.createRule = function createRule(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var rule = req.body;
    Rulebase.createRule(verbose, rule)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.createValue = function createValue(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var value = req.body;
    Rulebase.createValue(verbose, value)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.deleteCondition = function deleteCondition(req, res, next) {
    var name = req.swagger.params['name'].value;
    Rulebase.deleteCondition(name)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.deleteRule = function deleteRule(req, res, next) {
    var name = req.swagger.params['name'].value;
    Rulebase.deleteRule(name)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.deleteValue = function deleteValue(req, res, next) {
    var name = req.swagger.params['name'].value;
    Rulebase.deleteValue(name)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.getCondition = function getCondition(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Rulebase.getCondition(name, verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.getConditions = function getConditions(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Rulebase.getConditions(verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.getRule = function getRule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Rulebase.getRule(name, verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.getRulebaseService = function getRulebaseService(req, res, next) {
    Rulebase.getRulebaseService()
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.getRules = function getRules(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Rulebase.getRules(verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.getValue = function getValue(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Rulebase.getValue(name, verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.getValues = function getValues(req, res, next) {
    var verbose = req.swagger.params['verbose'].value;
    var depth = req.swagger.params['depth'].value;
    Rulebase.getValues(verbose, depth)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.replaceCondition = function replaceCondition(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var condition = req.body;
    Rulebase.replaceCondition(name, verbose, condition)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.replaceRule = function replaceRule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var rule = req.body;
    Rulebase.replaceRule(name, verbose, rule)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.replaceValue = function replaceValue(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var value = req.body;
    Rulebase.replaceValue(name, verbose, value)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.updateCondition = function updateCondition(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var condition = req.body;
    Rulebase.updateCondition(name, verbose, condition)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.updateRule = function updateRule(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var rule = req.body;
    Rulebase.updateRule(name, verbose, rule)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
module.exports.updateValue = function updateValue(req, res, next) {
    var name = req.swagger.params['name'].value;
    var verbose = req.swagger.params['verbose'].value;
    var value = req.body;
    Rulebase.updateValue(name, verbose, value)
        .then(function (response) {
        utils.writeJson(res, response);
    })
        .catch(function (response) {
        next(response);
    });
};
