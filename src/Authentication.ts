import User from './User'

export default interface Authentication {
  type:string
  user?:User
  token?:string
  key?:string
}

