export interface ClientErrorCodes {
    code: number
    message: string
}

export const Bad_Request = {code: 400, message: 'Bad Request'}
export const Unauthorized = {code: 401, message: 'Unauthorized'}
export const Payment_Required = {code: 402, message: 'Payment Required'}
export const Forbidden = {code: 403, message: 'Forbidden'}
export const Not_Found = {code: 404, message: 'Not Found'}

export default class ClientError extends Error {
    code: number

    constructor(cause:ClientErrorCodes, message?:string, ...params:any[]) {
        // Pass remaining arguments (including vendor specific ones) to parent constructor
        super(...params);
        this.code = cause.code

        // Maintains proper stack trace for where our error was thrown (only available on V8)
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, ClientError);
        }

        this.name = 'ClientError'
        if(message)
            this.message = message
        else
            this.message = cause.message
    }
}