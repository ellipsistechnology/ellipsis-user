enum RulebasePrivileges {
  getRules = 'Provides a list of black box rules.',
  createRule = 'Creates a rule.',
  getConditions = 'Provides a list of black box conditions.',
  createCondition = 'Creates a condition.',
  getValues = 'Provides a list of black box values.',
  createValue = 'Creates a value.',
  getRulebaseService = 'The blackbox rulebase service.'
}

export default RulebasePrivileges
