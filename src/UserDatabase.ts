import {named} from 'blackbox-ioc'
import User from './User';
import {Db} from 'mongodb'

import { dbGet, dbGetList, dbUpdate, dbCreate } from 'blackbox-database';
import { dbDelete } from 'blackbox-database';

const USER_COLLECTION = 'user'
const ROLE_COLLECTION = 'role'

async function ensurePrivileges(db:Db, _id:string, user:User) {
  if(user && !Array.isArray(user.privileges)) {
    if(Array.isArray(user.roles)) {
      let privs:string[] = []
      for(let roleName of user.roles) {
        const role = await db.collection(ROLE_COLLECTION).findOne({name:roleName})
        if(role)
          privs = privs.concat(role.privileges)
      }
      user.privileges = privs
    }
    else {
      user.privileges = []
    }
    await db.collection(USER_COLLECTION).replaceOne({username: user.username}, user)
  }
  return user
}

@named('user-database')
export default class UserDatabase {

  @dbGetList('User', async (db, users) => {
    for(let user of users){
      await ensurePrivileges(db, user.username, user)
    }
    return users.map((user:User) => Object.assign(new User(), user))
  })
  async getUserList(): Promise<User[]> { return <User[]>{} }

  @dbGet('User', 'username', async (db, username, user) => {
    await ensurePrivileges(db, username, user)
    return user
  })
  async getUser(_username:string): Promise<User> {  return <User><any>undefined }

  @dbCreate('User')
  async createUser(_user: User) {}

  @dbUpdate('User', 'username')
  async updateUser(_username:string, _user:User) {}

  @dbDelete('User', 'username')
  async deleteUser(_username:string) {}
}
