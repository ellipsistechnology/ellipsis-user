import {serviceClass, autowired, taggedService} from 'blackbox-ioc'
import {makeServiceObject} from 'blackbox-services'
import Role from './Role'
import {Service} from 'blackbox-services'
import {NamedReference} from 'blackbox-services'
import RoleDatabase from './RoleDatabase'

import UsersPrivileges from './UsersPrivileges';
import RulebasePrivileges from './RulebasePrivileges'; // TODO move to rules-service
import RolesPrivileges from './RolesPrivileges';

export const ADMIN_ROLE = 'Administrator'

@serviceClass('roles-service')
export class RolesServiceImpl {

  @autowired('oasDoc')
  oasDoc:any

  @autowired('role-database')
  rolesDb:RoleDatabase

  constructor() {
  }

  @taggedService('init', 'init-roles-service')
  async init() {
    // Ensure the admin role exists with all user privileges:
    if(!await this.rolesDb.getRole(ADMIN_ROLE)) {
      this.rolesDb.createRole({
        name: ADMIN_ROLE,
        privileges: Object.keys(UsersPrivileges)
            .concat(Object.keys(RulebasePrivileges))
            .concat(Object.keys(RolesPrivileges))
      })
      console.log(`Created Administrator role.`)
    }
  }

  async getRolesList(_props:any):Promise<Role[]> {
    return (await this.rolesDb.getRoleList()).map((role) => Object.assign(new Role(), role))
  }

  getOptionsForRolesService(_props:any):Service {
    return makeServiceObject(this.oasDoc, 'roles')
  }

  async createRoles({role}:any):Promise<NamedReference> {
    await this.rolesDb.createRole(role)
    return {name: role.name}
  }

  async getRole({name}:any):Promise<Role> {
    return Object.assign(new Role(), await this.rolesDb.getRole(name))
  }

  getOptionsForRole(_props:any):Service {
    return makeServiceObject(this.oasDoc, 'roles')
  }

  // FIXME need to invalidate users so privileges are reloaded
  async replaceRole({name, role}:any):Promise<NamedReference> {
    delete role._id
    await this.rolesDb.updateRole(name, role)
    return {name}
  }

  // FIXME need to invalidate users so privileges are reloaded
  async updateRole({name, role}:any):Promise<NamedReference> {
    const oldRole = await this.rolesDb.getRole(name)
    const newRole = Object.assign(oldRole, role)
    delete newRole._id
    await this.rolesDb.updateRole(name, newRole)
    return {name}
  }

  async deleteRole({name}:any):Promise<NamedReference> {
    await this.rolesDb.deleteRole(name)
    return {name}
  }
}
