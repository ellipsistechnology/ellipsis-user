enum RolesPrivileges {
  getRole = 'Gets a role by name.',
  getOptionsForRole = 'Gets a role by name.',
  replaceRoles = 'Replace the role identified by its name.',
  updateRole = 'Change some of the fields of the role identified by its name.',
  deleteRole = 'Delete the role identified by its name.',
  getRolesList = 'Retrieve a list of role objects.',
  getOptionsForRolesService = '',
  createRoles = 'Creates a new role.'
}

export default RolesPrivileges