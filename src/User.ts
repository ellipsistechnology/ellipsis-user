import { identifiedBy, hierarchical } from "blackbox-services";

@identifiedBy({id: 'username'})
@hierarchical({path: 'users'})
export default class User {
  username: string
  password?: string
  email?: string
  authTypes?: string[]
  roles?: string[]
  privileges?: string[]
}
