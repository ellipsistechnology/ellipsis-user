import {database} from 'blackbox-database'
import Role from './Role';
import {named} from 'blackbox-ioc';

@database()
@named('role-database')
export default class RoleDatabase {
  getRole(_name:string):Role {return <Role><unknown>undefined}
  getRoleList():Role[] {return <Role[]><unknown>undefined}
  createRole(_role:Role) {}
  updateRole(_name:string, _role:Role) {}
  deleteRole(_name:string) {}
}
