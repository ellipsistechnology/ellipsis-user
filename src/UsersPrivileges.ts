enum UsersPrivileges {
  getUser = 'Gets a user by name.',
  getOptionsForUser = 'Gets a users by name.',
  replaceUser = 'Replace the user identified by its name with the provided user.',
  updateUser = 'Change some of the fields of the user identified by its name.',
  deleteUsers = 'Delete the user identified by its name.',
  getUsersList = 'Retrieve a list of user objects.',
  getOptionsForUsersService = '',
  createUsers = 'Creates a new user.'
}

export default UsersPrivileges