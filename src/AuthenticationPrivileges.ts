enum AuthenticationPrivileges {
  getAuthenticationList = 'Retrieve a list of authentication objects.',
  getOptionsForAuthenticationService = '',
  createAuthentication = 'Creates a new authentication of type authentication.'
}

export default AuthenticationPrivileges