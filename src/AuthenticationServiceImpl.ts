import {serviceClass, autowired} from 'blackbox-ioc'
import Authentication from './Authentication'
import {NamedReference} from 'blackbox-services'
import jwt from 'jsonwebtoken'
import UserDatabase from './UserDatabase'
import fs from 'fs'
import bcrypt from 'bcrypt'
import {makeServiceObject} from 'blackbox-services'
import {Service} from 'blackbox-services'

export const SALT_ROUNDS = 10

const {OAuth2Client} = require('google-auth-library')
import credentials from './credentials.json'
import User from './User'
import ClientError, { Unauthorized } from './ClientError';

@serviceClass('authentication-service')
export class AuthenticationServiceImpl {

  @autowired('oasDoc')
  oasDoc:any

  @autowired('user-database')
  userdb:UserDatabase

  googelClient:any

  constructor() {
    this.googelClient = new OAuth2Client(credentials.web.client_id);
  }

  getAuthenticationList(props:any):Authentication[] {
    let publicKey = fs.readFileSync('key.pub').toString();
    if(publicKey.endsWith('\n'))
      publicKey = publicKey.substring(0, publicKey.length-1)
    return [{
      type: "public-key",
      key: publicKey.toString()
    }]
  }

  private async authenticateByPassword(authentication: Authentication):Promise<User> {
    if (!authentication.user || !authentication.user.username || !authentication.user.password)
      throw new ClientError(Unauthorized, `user object with username and password must be provided when authenticating with password.`);

    const user = await this.userdb.getUser(authentication.user.username);
    if (!user ||
      !Array.isArray(user.authTypes) ||
      !(<any>user.authTypes).includes('password') ||
      !user.password ||
      !await bcrypt.compare(authentication.user.password, user.password)
    ) {
      throw new ClientError(Unauthorized, `Invalid username or password`);
    }

    return user
  }

  private async authenticateByGoogle(authentication: Authentication):Promise<User> {
    if(!authentication.token || !authentication.user)
      throw new ClientError(Unauthorized, `token and user must be provided when authenticating with Google.`)

    const user = await this.userdb.getUser(authentication.user.username)
    if(!user || !Array.isArray(user.authTypes) || !(<any>user.authTypes).includes('google'))
      throw new ClientError(Unauthorized, `Invalid token for user.`)

    try {
      const ticket = await this.googelClient.verifyIdToken({
          idToken: authentication.token,
          audience: credentials.web.client_id
      });
      const payload = ticket.getPayload();
      const username = payload['sub'];
      if(username !== user.username)
        throw new Error(`Invalid token for user.`)

      // TODO lazily setup user and profile (profile may be another microservice, e.g. {
      //   user: User or string for username
      //   person: Person
      //   profile details specific to the site using the service
      // }

      return user
    }
    catch(err) {
      throw new ClientError(Unauthorized, err.toString())
    }
  }

  async createAuthentication({authentication}:{authentication:Authentication}):Promise<NamedReference&Authentication> {
    let user
    switch(authentication.type) {
    case 'password':
      user = await this.authenticateByPassword(authentication);
      break;
    case 'google':
      user = await this.authenticateByGoogle(authentication)
      break;
    default:
      throw new ClientError(Unauthorized, `Invalid authentication type: type: ${authentication.type}`)
    }

    let privateKey: any = fs.readFileSync('key');
    const payload = {
        sub: user.username,
        priv: user.privileges,
        iss: "bb",
    };

    var token = jwt.sign(payload, privateKey, { algorithm: 'RS256' });
    return {
        type: 'jwt',
        name: user.username.toString(),
        token
    };
  }

  getOptionsForAuthenticationService(props:any):Service {
    return makeServiceObject(this.oasDoc, 'authentication')
  }

  getAuthentication(props:any):Authentication {
    throw new Error(`getAuthentication not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  getOptionsForAuthentication(props:any):Service {
    return makeServiceObject(this.oasDoc, 'authentication')
  }

  replaceAuthentication(props:any):NamedReference {
    throw new Error(`replaceAuthentication not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  updateAuthentication(props:any):NamedReference {
    throw new Error(`updateAuthentication not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  deleteAuthentication(props:any):NamedReference {
    throw new Error(`deleteAuthentication not yet implemented. Called with props=${JSON.stringify(props)}`)
  }
}
