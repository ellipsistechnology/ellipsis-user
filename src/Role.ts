import { hierarchical } from "blackbox-services";

@hierarchical({path: 'roles'})
export default class Role {
  name:string
  privileges:string[]
}
