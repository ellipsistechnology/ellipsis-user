import ClientError, {Unauthorized, Forbidden} from "./ClientError";
import { verify } from 'jsonwebtoken';
import fs from 'fs';

export const authenticationMiddleware = ({ignore}:{ignore?:string[]} = {}) => (req:any, res:any, next:any) => {
  if((<any>ignore).includes(req.path))
    return next()

  if(req.headers.authorization) {
    let token:any = (<string>req.headers.authorization).match(/(?<=Bearer\s)[^\s]*/)
    if(token) {
      try {
        const publicKey = fs.readFileSync('key.pub').toString()
        const {priv, sub} = <any>verify(token.toString(), publicKey, { algorithms: ['RS256'], ignoreExpiration: true }) // TODO don't ignore expiration

        if(!Array.isArray(priv) || (typeof sub !== 'string') || (<string>sub).length < 1)
          return next(new ClientError(Unauthorized, 'Invalid token.'))

        req.user = {
          username: sub,
          privileges: priv
        }
        return next()
      }
      catch(err) {
        console.error(err)
        return next(new ClientError(Unauthorized, 'Invalid token.'));
      }
    }
    else {
      return next(new ClientError(Forbidden, 'Authorization required.'));
    }
  }
  else {
    return next(new ClientError(Forbidden, 'Authorization required.'));
  }
}

export const authorisationMiddleware = ({ignore}:{ignore?:string[]} = {}) => (req:any, res:any, next:any) => {
  if((<any>ignore).includes(req.path))
    return next()

  try {
    if(!req.swagger || !req.swagger.operation) // let the router handle the lack of support for this method
      return next()
    const {operationId} = req.swagger.operation
    if(!req.user.privileges.includes(operationId))
      return next(new ClientError(Unauthorized, `User ${req.user.username} does not have required permission: ${operationId}.`))
  }
  catch(err) {
    return next(err)
  }

  next()
}
