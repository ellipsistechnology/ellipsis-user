import {serviceClass, autowired, taggedService} from 'blackbox-ioc'
import {makeServiceObject, verbose} from 'blackbox-services'
import User from './User'
import {Service} from 'blackbox-services'
import {NamedReference} from 'blackbox-services'
import UserDatabase from './UserDatabase';
import bcrypt from 'bcrypt'
import {SALT_ROUNDS} from './AuthenticationServiceImpl'
import {ADMIN_ROLE} from './RolesServiceImpl'
import {DataClient} from 'blackbox-client'

const ADMIN_USERNAME = 'admin'
const DEFAULT_ADMIN_PASSWORD = 'dotdotdot'

const ARCHIVE_SERVICE_URL = "http://localhost:8081/archive/data" // TODO move to config
const USER_SERVICE_URL = "http://localhost:8088/users"

@serviceClass('users-service')
export class UsersServiceImpl {

  @autowired('oasDoc')
  oasDoc: any

  @autowired('user-database')
  userdb: UserDatabase

  constructor() {
    if(!this.userdb)
      throw new Error(`'user-database' service not found`)
  }

  @taggedService('init', 'init-users-service')
  async init() {
    // Ensure the admin user exists:
    const adminUser = await this.userdb.getUser(ADMIN_USERNAME);
    if(!adminUser) {
      this.userdb.createUser({
        username: ADMIN_USERNAME,
        password: await bcrypt.hash(DEFAULT_ADMIN_PASSWORD, SALT_ROUNDS),
        roles: [ADMIN_ROLE],
        authTypes: ['password']
      })
      console.log(`Created admin user - default password set to '${DEFAULT_ADMIN_PASSWORD}' - CHANGE PASSWORD BEFORE DEPLOYING TO PRODUCTION!`)
    }
  }

  async getUsersList({verbose}:any): Promise<User[]> {
    return (await this.userdb.getUserList())
      .map(({password, ...userWithoutPassword}:User) => (
        Object.assign(new User(), this.makeUserVerbose(verbose, userWithoutPassword))
      ))
  }

  getOptionsForUsersService(_props:any): Service {
    return makeServiceObject(this.oasDoc, 'users')
  }

  private async hashPassword(user: User) {
    user.password = await bcrypt.hash(user.password, SALT_ROUNDS)
  }

  // TODO allow for multiple users
  async createUsers({user}:any):Promise<NamedReference> {
    await this.hashPassword(user)
    this.userdb.createUser(user)
    return {name: user.username}
  }

  private makeUserVerbose(v:boolean, user:User): User {
    if(v) {
      const roles = user.roles ?
        user.roles.reduce((map:any, role:string) => {
          map[role] = {
            href: '/roles/'+role
          }
          return map
        }, {})
        : {}
      roles.href = '/roles'
      roles.description = "The user's roles."
      verbose({links: { roles }})(user)
    }
    return user
  }

  async getUser({name, verbose}:any): Promise<User> {
    const {password, ...userWithoutPassword} = await this.userdb.getUser(name);
    const user = Object.assign(new User(), userWithoutPassword);

    return this.makeUserVerbose(verbose, user)
  }

  getOptionsForUser(_props:any): Service {
    return makeServiceObject(this.oasDoc, 'users')
  }

  async replaceUser({name, user}:{name: string, user: User}): Promise<NamedReference> {
    if(!user.password) {
      const oldUser = await this.userdb.getUser(name)
      user.password = oldUser.password
    } else {
      await this.hashPassword(user)
    }
    user.privileges = undefined
    delete (<any>user)._id
    await this.userdb.updateUser(name, user)
    return {name: user.username}
  }

  async updateUser({name, user}:any): Promise<NamedReference> {
    const oldUser = await this.userdb.getUser(name)
    const newUser = Object.assign(oldUser, user)
    delete newUser._id
    newUser.privileges = undefined
    await this.hashPassword(newUser)
    await this.userdb.updateUser(name, newUser)
    return {name: newUser.username}
  }

  async deleteUser({name}:any): Promise<NamedReference&{archiveId: string}> {
    const user = await this.userdb.getUser(name)
    delete (<any>user)._id
    const id = (await new DataClient(ARCHIVE_SERVICE_URL).post({
      data: user,
      type: 'delete',
      serviceUrl: USER_SERVICE_URL
    })).name
    await this.userdb.deleteUser(name)
    return {name, archiveId: id}
  }
}
