import {serviceClass, autowired} from 'blackbox-ioc'
import {makeServiceObject} from 'blackbox-services'
import Role from './Role'
import {NamedReference} from 'blackbox-services'
import Privilege from './Privilege'


@serviceClass('security-service')
export class SecurityServiceImpl {

  @autowired('oasDoc')
  oasDoc:any

  constructor() {
  }

  
  getSecurity(props:any) {
    throw new Error(`getSecurity not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  getRoles(props:any):Role[] {
    throw new Error(`getRoles not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  createRole(props:any):NamedReference {
    throw new Error(`createRole not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  getRole(props:any):Role {
    throw new Error(`getRole not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  replaceRole(props:any):NamedReference {
    throw new Error(`replaceRole not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  updateRole(props:any):NamedReference {
    throw new Error(`updateRole not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  deleteRole(props:any):NamedReference {
    throw new Error(`deleteRole not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  getPrivileges(props:any):Privilege[] {
    throw new Error(`getPrivileges not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  createPrivilege(props:any):NamedReference {
    throw new Error(`createPrivilege not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  getPrivilege(props:any):Privilege {
    throw new Error(`getPrivilege not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  replacePrivilege(props:any):NamedReference {
    throw new Error(`replacePrivilege not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  updatePrivilege(props:any):NamedReference {
    throw new Error(`updatePrivilege not yet implemented. Called with props=${JSON.stringify(props)}`)
  }

  deletePrivilege(props:any):NamedReference {
    throw new Error(`deletePrivilege not yet implemented. Called with props=${JSON.stringify(props)}`)
  }
}
