const { parallel, src, dest } = require('gulp');
var ts = require('gulp-typescript');

var tsProject = ts.createProject('tsconfig.json');

function copyApi() {
  return src('gensrc/api/*')
    .pipe(dest('dist/api/'));
}

function copyLogin() {
  return src('login.html')
    .pipe(dest('dist'));
}

function copyConfig() {
  return src('src/*.json')
    .pipe(dest('dist/src'));
}

function copyFavicon() {
  return src('node_modules/ellipsis-favicon/dist/favicon.ico')
    .pipe(dest('dist'));
}

function build() {
  var tsResult = tsProject.src()
      .pipe(tsProject());

  return tsResult.js.pipe(dest('dist'));
}

exports.default = parallel(copyApi, copyLogin, copyConfig, copyFavicon, build)
