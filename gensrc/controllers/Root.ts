'use strict';

var utils = require('../utils/writer.js');
var Root = require('../service/RootService');

module.exports.getRootService = function getRootService (req:any, res:any, next:any) {
  Root.getRootService()
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};
