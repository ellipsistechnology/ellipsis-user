'use strict';

var utils = require('../utils/writer.js');
var Authentication = require('../service/AuthenticationService');

module.exports.createAuthentication = function createAuthentication (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var authentication = req.body;
  Authentication.createAuthentication(verbose,authentication)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getAuthenticationList = function getAuthenticationList (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  Authentication.getAuthenticationList(depth,verbose)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getOptionsForAuthenticationService = function getOptionsForAuthenticationService (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  Authentication.getOptionsForAuthenticationService(depth,verbose)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};
