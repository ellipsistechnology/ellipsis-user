'use strict';

var utils = require('../utils/writer.js');
var Roles = require('../service/RolesService');

module.exports.createRoles = function createRoles (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var role = req.body;
  Roles.createRoles(verbose,role)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.deleteRole = function deleteRole (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  Roles.deleteRole(name)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getOptionsForRole = function getOptionsForRole (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  Roles.getOptionsForRole(name,verbose,depth)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getOptionsForRolesService = function getOptionsForRolesService (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  Roles.getOptionsForRolesService(depth,verbose)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getRole = function getRole (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  Roles.getRole(name,verbose,depth)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getRolesList = function getRolesList (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  Roles.getRolesList(depth,verbose)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.replaceRoles = function replaceRoles (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var role = req.body;
  Roles.replaceRoles(name,verbose,role)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.updateRole = function updateRole (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var role = req.body;
  Roles.updateRole(name,verbose,role)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};
