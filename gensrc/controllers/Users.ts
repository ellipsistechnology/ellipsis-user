'use strict';

var utils = require('../utils/writer.js');
var Users = require('../service/UsersService');

module.exports.createUsers = function createUsers (req:any, res:any, next:any) {
  var verbose = req.swagger.params['verbose'].value;
  var user = req.body;
  Users.createUsers(verbose,user)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.deleteUsers = function deleteUsers (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  Users.deleteUsers(name)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getOptionsForUser = function getOptionsForUser (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  Users.getOptionsForUser(name,verbose,depth)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getOptionsForUsersService = function getOptionsForUsersService (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  Users.getOptionsForUsersService(depth,verbose)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getUser = function getUser (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var depth = req.swagger.params['depth'].value;
  Users.getUser(name,verbose,depth)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.getUsersList = function getUsersList (req:any, res:any, next:any) {
  var depth = req.swagger.params['depth'].value;
  var verbose = req.swagger.params['verbose'].value;
  Users.getUsersList(depth,verbose)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.replaceUser = function replaceUser (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var user = req.body;
  Users.replaceUser(name,verbose,user)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};

module.exports.updateUser = function updateUser (req:any, res:any, next:any) {
  var name = req.swagger.params['name'].value;
  var verbose = req.swagger.params['verbose'].value;
  var user = req.body;
  Users.updateUser(name,verbose,user)
    .then(function (response:any) {
      utils.writeJson(res, response);
    })
    .catch(function (response:any) {
      next(response);
    });
};
