'use strict';

import {autowiredService} from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class RulebaseServiceWrapper {
  @autowiredService('rulebase-service')
  service: any
}
const rulebaseWrapper = new RulebaseServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}


/**
 * Creates a condition.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * condition Condition A blackbox condition. (optional)
 * returns named-reference
 **/
exports.createCondition = async function(verbose:any,condition:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for createCondition:
  let val = await Promise.resolve(rulebaseWrapper.service.createCondition({
    verbose:verbose,
    condition:condition
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Creates a rule.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * rule Rule A blackbox rule. (optional)
 * returns named-reference
 **/
exports.createRule = async function(verbose:any,rule:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for createRule:
  let val = await Promise.resolve(rulebaseWrapper.service.createRule({
    verbose:verbose,
    rule:rule
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Creates a value.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * value Value A blackbox value. (optional)
 * returns named-reference
 **/
exports.createValue = async function(verbose:any,value:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for createValue:
  let val = await Promise.resolve(rulebaseWrapper.service.createValue({
    verbose:verbose,
    value:value
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Delete the condition identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteCondition = async function(name:any) {

  // Call service provider for deleteCondition:
  let val = await Promise.resolve(rulebaseWrapper.service.deleteCondition({
    name:name
  }))

  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "name" : "name"
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Delete the rule identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteRule = async function(name:any) {

  // Call service provider for deleteRule:
  let val = await Promise.resolve(rulebaseWrapper.service.deleteRule({
    name:name
  }))

  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "name" : "name"
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Delete the value identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteValue = async function(name:any) {

  // Call service provider for deleteValue:
  let val = await Promise.resolve(rulebaseWrapper.service.deleteValue({
    name:name
  }))

  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "name" : "name"
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Request a condition identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns condition
 **/
exports.getCondition = async function(name:any,verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getCondition:
  let val = await Promise.resolve(rulebaseWrapper.service.getCondition({
    name:name,
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Provides a list of black box conditions.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns List
 **/
exports.getConditions = async function(verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getConditions:
  let val = await Promise.resolve(rulebaseWrapper.service.getConditions({
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = "";
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Request a rule identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns rule
 **/
exports.getRule = async function(name:any,verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getRule:
  let val = await Promise.resolve(rulebaseWrapper.service.getRule({
    name:name,
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * The blackbox rulebase service.
 *
 * returns service
 **/
exports.getRulebaseService = async function() {

  // Call service provider for getRulebaseService:
  let val = await Promise.resolve(rulebaseWrapper.service.getRulebaseService({
    
  }))

  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "bbversion" : "bbversion",
  "description" : "description",
  "links" : {
    "key" : ""
  }
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Provides a list of black box rules.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns List
 **/
exports.getRules = async function(verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getRules:
  let val = await Promise.resolve(rulebaseWrapper.service.getRules({
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = "";
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Request a value identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns value
 **/
exports.getValue = async function(name:any,verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getValue:
  let val = await Promise.resolve(rulebaseWrapper.service.getValue({
    name:name,
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Provides a list of black box values.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns List
 **/
exports.getValues = async function(verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getValues:
  let val = await Promise.resolve(rulebaseWrapper.service.getValues({
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = "";
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Replace the condition identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * condition Condition A Blackbox condition. (optional)
 * returns named-reference
 **/
exports.replaceCondition = async function(name:any,verbose:any,condition:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for replaceCondition:
  let val = await Promise.resolve(rulebaseWrapper.service.replaceCondition({
    name:name,
    verbose:verbose,
    condition:condition
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Replace the rule identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * rule Rule A Blackbox rule. (optional)
 * returns named-reference
 **/
exports.replaceRule = async function(name:any,verbose:any,rule:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for replaceRule:
  let val = await Promise.resolve(rulebaseWrapper.service.replaceRule({
    name:name,
    verbose:verbose,
    rule:rule
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Replace the value identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * value Value A Blackbox value. (optional)
 * returns named-reference
 **/
exports.replaceValue = async function(name:any,verbose:any,value:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for replaceValue:
  let val = await Promise.resolve(rulebaseWrapper.service.replaceValue({
    name:name,
    verbose:verbose,
    value:value
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Change some of the fields of the condition identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * condition Condition A Blackbox condition. (optional)
 * returns named-reference
 **/
exports.updateCondition = async function(name:any,verbose:any,condition:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for updateCondition:
  let val = await Promise.resolve(rulebaseWrapper.service.updateCondition({
    name:name,
    verbose:verbose,
    condition:condition
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Change some of the fields of the rule identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * rule Rule A Blackbox rule. (optional)
 * returns named-reference
 **/
exports.updateRule = async function(name:any,verbose:any,rule:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for updateRule:
  let val = await Promise.resolve(rulebaseWrapper.service.updateRule({
    name:name,
    verbose:verbose,
    rule:rule
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Change some of the fields of the value identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * value Value A Blackbox value. (optional)
 * returns named-reference
 **/
exports.updateValue = async function(name:any,verbose:any,value:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for updateValue:
  let val = await Promise.resolve(rulebaseWrapper.service.updateValue({
    name:name,
    verbose:verbose,
    value:value
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}

