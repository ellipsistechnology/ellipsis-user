'use strict';

import {autowiredService} from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class AuthenticationServiceWrapper {
  @autowiredService('authentication-service')
  service: any
}
const authenticationWrapper = new AuthenticationServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}


/**
 * Creates a new authentication.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * authentication Authentication A authentication. (optional)
 * returns named-reference
 **/
exports.createAuthentication = async function(verbose:any,authentication:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for createAuthentication:
  let val = await Promise.resolve(authenticationWrapper.service.createAuthentication({
    verbose:verbose,
    authentication:authentication
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Retrieve a list of authentication objects.
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns List
 **/
exports.getAuthenticationList = async function(depth:any,verbose:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getAuthenticationList:
  let val = await Promise.resolve(authenticationWrapper.service.getAuthenticationList({
    depth:depth,
    verbose:verbose
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = "";
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns service
 **/
exports.getOptionsForAuthenticationService = async function(depth:any,verbose:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getOptionsForAuthenticationService:
  let val = await Promise.resolve(authenticationWrapper.service.getOptionsForAuthenticationService({
    depth:depth,
    verbose:verbose
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "bbversion" : "bbversion",
  "description" : "description",
  "links" : {
    "key" : ""
  }
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}

