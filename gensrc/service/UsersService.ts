'use strict';

import {autowiredService} from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class UsersServiceWrapper {
  @autowiredService('users-service')
  service: any
}
const usersWrapper = new UsersServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}


/**
 * Creates a new user.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * user User A user. (optional)
 * returns named-reference
 **/
exports.createUsers = async function(verbose:any,user:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for createUsers:
  let val = await Promise.resolve(usersWrapper.service.createUsers({
    verbose:verbose,
    user:user
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Delete the user identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteUsers = async function(name:any) {

  // Call service provider for deleteUsers:
  let val = await Promise.resolve(usersWrapper.service.deleteUser({
    name:name
  }))

  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "name" : "name"
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Gets a users by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns service
 **/
exports.getOptionsForUser = async function(name:any,verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getOptionsForUser:
  let val = await Promise.resolve(usersWrapper.service.getOptionsForUser({
    name:name,
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "bbversion" : "bbversion",
  "description" : "description",
  "links" : {
    "key" : ""
  }
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns service
 **/
exports.getOptionsForUsersService = async function(depth:any,verbose:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getOptionsForUsersService:
  let val = await Promise.resolve(usersWrapper.service.getOptionsForUsersService({
    depth:depth,
    verbose:verbose
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "bbversion" : "bbversion",
  "description" : "description",
  "links" : {
    "key" : ""
  }
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Gets a user by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns user
 **/
exports.getUser = async function(name:any,verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getUser:
  let val = await Promise.resolve(usersWrapper.service.getUser({
    name:name,
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Retrieve a list of user objects.
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns List
 **/
exports.getUsersList = async function(depth:any,verbose:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getUsersList:
  let val = await Promise.resolve(usersWrapper.service.getUsersList({
    depth:depth,
    verbose:verbose
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = "";
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Replace the user identified by its name with the provided user.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * user User A user. (optional)
 * returns named-reference
 **/
exports.replaceUser = async function(name:any,verbose:any,user:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for replaceUser:
  let val = await Promise.resolve(usersWrapper.service.replaceUser({
    name:name,
    verbose:verbose,
    user:user
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Change some of the fields of the user identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * user User A user. (optional)
 * returns named-reference
 **/
exports.updateUser = async function(name:any,verbose:any,user:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for updateUser:
  let val = await Promise.resolve(usersWrapper.service.updateUser({
    name:name,
    verbose:verbose,
    user:user
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}

