'use strict';

import {autowiredService} from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class RolesServiceWrapper {
  @autowiredService('roles-service')
  service: any
}
const rolesWrapper = new RolesServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}


/**
 * Creates a new role.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.createRoles = async function(verbose:any,role:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for createRoles:
  let val = await Promise.resolve(rolesWrapper.service.createRoles({
    verbose:verbose,
    role:role
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Delete the role identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteRole = async function(name:any) {

  // Call service provider for deleteRole:
  let val = await Promise.resolve(rolesWrapper.service.deleteRole({
    name:name
  }))

  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "name" : "name"
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Gets a role by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns service
 **/
exports.getOptionsForRole = async function(name:any,verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getOptionsForRole:
  let val = await Promise.resolve(rolesWrapper.service.getOptionsForRole({
    name:name,
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "bbversion" : "bbversion",
  "description" : "description",
  "links" : {
    "key" : ""
  }
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns service
 **/
exports.getOptionsForRolesService = async function(depth:any,verbose:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getOptionsForRolesService:
  let val = await Promise.resolve(rolesWrapper.service.getOptionsForRolesService({
    depth:depth,
    verbose:verbose
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "bbversion" : "bbversion",
  "description" : "description",
  "links" : {
    "key" : ""
  }
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Gets a role by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns role
 **/
exports.getRole = async function(name:any,verbose:any,depth:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getRole:
  let val = await Promise.resolve(rolesWrapper.service.getRole({
    name:name,
    verbose:verbose,
    depth:depth
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Retrieve a list of role objects.
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns List
 **/
exports.getRolesList = async function(depth:any,verbose:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for getRolesList:
  let val = await Promise.resolve(rolesWrapper.service.getRolesList({
    depth:depth,
    verbose:verbose
  }))

  val = verboseResponse(val, verbose)
  val = trimToDepth(val, depth)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = "";
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Replace the role identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.replaceRoles = async function(name:any,verbose:any,role:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for replaceRoles:
  let val = await Promise.resolve(rolesWrapper.service.replaceRole({
    name:name,
    verbose:verbose,
    role:role
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}


/**
 * Change some of the fields of the role identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.updateRole = async function(name:any,verbose:any,role:any) {
  verbose = convertBooleanParameter(verbose)

  // Call service provider for updateRole:
  let val = await Promise.resolve(rolesWrapper.service.updateRole({
    name:name,
    verbose:verbose,
    role:role
  }))

  val = verboseResponse(val, verbose)
  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}

