'use strict';

import {autowiredService} from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class SecurityServiceWrapper {
  @autowiredService('security-service')
  service: any
}
const securityWrapper = new SecurityServiceWrapper()


/**
 * Creates a new privilege of type privilege.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * privilege Privilege A privilege. (optional)
 * returns named-reference
 **/
exports.createPrivilege = function(verbose:any,privilege:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for createPrivilege:
    try {
      let val = securityWrapper.service.createPrivilege({
        verbose:verbose,
        privilege:privilege
      })

      val = verboseResponse(val, verbose)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Creates a new role of type role.
 *
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.createRole = function(verbose:any,role:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for createRole:
    try {
      let val = securityWrapper.service.createRole({
        verbose:verbose,
        role:role
      })

      val = verboseResponse(val, verbose)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Delete the privilege identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deletePrivilege = function(name:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for deletePrivilege:
    try {
      let val = securityWrapper.service.deletePrivilege({
        name:name
      })

      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    examples['application/json'] = {
  "name" : "name"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Delete the role identified by its name.
 *
 * name String The object's name.
 * returns named-reference
 **/
exports.deleteRole = function(name:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for deleteRole:
    try {
      let val = securityWrapper.service.deleteRole({
        name:name
      })

      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    examples['application/json'] = {
  "name" : "name"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Gets a privilege by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns privilege
 **/
exports.getPrivilege = function(name:any,verbose:any,depth:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for getPrivilege:
    try {
      let val = securityWrapper.service.getPrivilege({
        name:name,
        verbose:verbose,
        depth:depth
      })

      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Gets a privilege by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns privilege
 **/
exports.getPrivilege_0 = function(name:any,verbose:any,depth:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for getPrivilege_0:
    try {
      let val = securityWrapper.service.getPrivilege_0({
        name:name,
        verbose:verbose,
        depth:depth
      })

      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Retrieve a list of privilege objects.
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns List
 **/
exports.getPrivileges = function(depth:any,verbose:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for getPrivileges:
    try {
      let val = securityWrapper.service.getPrivileges({
        depth:depth,
        verbose:verbose
      })

      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Gets a role by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns role
 **/
exports.getRole = function(name:any,verbose:any,depth:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for getRole:
    try {
      let val = securityWrapper.service.getRole({
        name:name,
        verbose:verbose,
        depth:depth
      })

      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Gets a role by name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * returns role
 **/
exports.getRole_0 = function(name:any,verbose:any,depth:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for getRole_0:
    try {
      let val = securityWrapper.service.getRole_0({
        name:name,
        verbose:verbose,
        depth:depth
      })

      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Retrieve a list of role objects.
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns List
 **/
exports.getRoles = function(depth:any,verbose:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for getRoles:
    try {
      let val = securityWrapper.service.getRoles({
        depth:depth,
        verbose:verbose
      })

      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    examples['application/json'] = "";
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 *
 * depth BigDecimal Indicates the depth of the returned object's hierarchy. (optional)
 * verbose String Indicates that the response must have verbose output. (optional)
 * returns service
 **/
exports.getSecurity = function(depth:any,verbose:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for getSecurity:
    try {
      let val = securityWrapper.service.getSecurity({
        depth:depth,
        verbose:verbose
      })

      val = verboseResponse(val, verbose)
      val = trimToDepth(val, depth)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    examples['application/json'] = {
  "bbversion" : "bbversion",
  "description" : "description",
  "links" : {
    "key" : ""
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Replace the privilege identified by its name with the provided privilege.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * privilege Privilege A privilege. (optional)
 * returns named-reference
 **/
exports.replacePrivilege = function(name:any,verbose:any,privilege:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for replacePrivilege:
    try {
      let val = securityWrapper.service.replacePrivilege({
        name:name,
        verbose:verbose,
        privilege:privilege
      })

      val = verboseResponse(val, verbose)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Replace the role identified by its name with the provided role.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.replaceRole = function(name:any,verbose:any,role:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for replaceRole:
    try {
      let val = securityWrapper.service.replaceRole({
        name:name,
        verbose:verbose,
        role:role
      })

      val = verboseResponse(val, verbose)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Change some of the fields of the privilege identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * privilege Privilege A privilege. (optional)
 * returns named-reference
 **/
exports.updatePrivilege = function(name:any,verbose:any,privilege:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for updatePrivilege:
    try {
      let val = securityWrapper.service.updatePrivilege({
        name:name,
        verbose:verbose,
        privilege:privilege
      })

      val = verboseResponse(val, verbose)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}


/**
 * Change some of the fields of the role identified by its name.
 *
 * name String The object's name.
 * verbose String Indicates that the response must have verbose output. (optional)
 * role Role A role. (optional)
 * returns named-reference
 **/
exports.updateRole = function(name:any,verbose:any,role:any) {
  return new Promise(function(resolve, reject) {
    // Call service provider for updateRole:
    try {
      let val = securityWrapper.service.updateRole({
        name:name,
        verbose:verbose,
        role:role
      })

      val = verboseResponse(val, verbose)
      resolve(val)
    }
    catch(err) {
      console.error(err)
      if(typeof err === 'string')
        reject(err)

      if(err instanceof Error)
        reject({
          error:{
            name:err.name,
            description:err.message
          }
        })
      else
        reject({error:err})
    }

    // Return value:
    /* TODO: Allow for examples when using OPTIONS or
    var examples = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
    */
  });
}

