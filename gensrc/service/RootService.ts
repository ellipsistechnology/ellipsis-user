'use strict';

import {autowiredService} from 'blackbox-ioc'
import { trimToDepth, verboseResponse } from 'blackbox-services';

class RootServiceWrapper {
  @autowiredService('root-service')
  service: any
}
const rootWrapper = new RootServiceWrapper()

function strip__(target: any): any {
  if(Array.isArray(target)) {
    return target.map(strip__)
  } else {
    const ret:any = {}
    Object.keys(target).forEach(key => {
      if(!key.startsWith('__'))
        ret[key] = target[key]
    })
    return ret
  }
}

function convertBooleanParameter(val:any):boolean {
  if(val === 'false')
       return false;
  if(val === '')
     return true;
  return val
}


/**
 * A list of services provided by this API.
 *
 * returns service
 **/
exports.getRootService = async function() {

  // Call service provider for getRootService:
  let val = await Promise.resolve(rootWrapper.service.getRootService({
    
  }))

  return strip__(val)

  // Return value:
  /* TODO: Allow for examples when using OPTIONS or
  var examples = {};
  examples['application/json'] = {
  "bbversion" : "bbversion",
  "description" : "description",
  "links" : {
    "key" : ""
  }
};
  if (Object.keys(examples).length > 0) {
    resolve(examples[Object.keys(examples)[0]]);
  } else {
    resolve();
  }
  */
}

