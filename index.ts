/*
 * Blackbox API for user management, authentication and authroisation.
 * TODO Convert the below procedural approach to fully OO.
 */

'use strict';

import {factory, autowired, callServices, taggedService} from 'blackbox-ioc'
import {DefaultRuleBase} from 'blackbox-rules-utils'
import RuleBase from 'blackbox-rules'
import {configureDatabase} from 'blackbox-database'
import initRulesService from 'blackbox-rules-service'
import initRootServices from 'blackbox-root-service'
import cors from 'cors';

import { RolesServiceImpl } from './src/RolesServiceImpl';
import { UsersServiceImpl } from './src/UsersServiceImpl';
import { authenticationMiddleware, authorisationMiddleware } from './src/securityMiddleware';
import ClientError from './src/ClientError';

import {loadConfig} from 'blackbox-config'

const config = loadConfig()
if(config.databases && config.databases.user) {
  configureDatabase(config.databases.user) // Must be called before any database decorations.
}

var fs = require('fs'),
    path = require('path'),
    http = require('http'),
    glob = require('glob'),
    express = require('express'),
    oas3Tools = require('oas3-tools'),
    jsyaml = require('js-yaml'),
    serverPort = 8088;

const app = express()

// Load all files to ensure decorations are read for IOC:
glob
  .sync(`${__dirname}/*src/**/*.js`)
  .forEach( (file:string) => {
    require(path.resolve(file))
  } )

class AppInitialiser {
  swaggerDoc:any

  @autowired('roles-service')
  rolesService: RolesServiceImpl

  @autowired('users-service')
  usersService: UsersServiceImpl

  @factory('rulebase')
  createRuleBase():RuleBase {
    return new DefaultRuleBase()
  }

  @factory('oasDoc')
  getOasDoc() {
    if(!this.swaggerDoc) {
      var spec = fs.readFileSync(path.join(__dirname,'api/openapi.yaml'), 'utf8');
      this.swaggerDoc = jsyaml.safeLoad(spec);
    }
    return this.swaggerDoc
  }

  @taggedService('init', 'init-services')
  init() {
    initRulesService()
    initRootServices()

    // swaggerRouter configuration
    var options = {
      swaggerUi: path.join(__dirname, '/openapi.json'),
      controllers: path.join(__dirname, './gensrc/controllers'),
      useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
    };

    // Initialize the Swagger middleware
    oas3Tools.initializeMiddleware(this.getOasDoc(), function (middleware:any) {

      app.use(cors({
        "methods": "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS"
      }));

      app.options('/users', function(req:any, res:any) {
        console.log('test')
      })

      app.get('/login.html', function(req:any, res:any) {
        res.sendFile(path.join(__dirname + '/login.html'));
      })

      app.get('/favicon.ico', function(req:any, res:any) {
        res.sendFile(path.join(__dirname + '/favicon.ico'));
      })

      app.use(authenticationMiddleware({ignore: ['/authentication', '/authentication/', '/']}))

      // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
      app.use(middleware.swaggerMetadata());

      // Validate Swagger requests
      app.use(middleware.swaggerValidator());

      app.use(authorisationMiddleware({ignore: ['/authentication', '/authentication/', '/']}))

      // Handle security errors (before swagger handles it):
      app.use(clientErrorMiddleware)

      // Route validated requests to appropriate controller
      app.use(middleware.swaggerRouter(options))

      // Handle client errors thrown by service implementations:
      app.use(clientErrorMiddleware)

      // Serve the Swagger documents and Swagger UI
      app.use(middleware.swaggerUi())

      // TODO better error handler or fix the inbuilt one that the htt server is providing
      // app.use(function errorHandler(err:any, req:any, res:any, next:any) {
      //   // default to 500 server error
      //   return res.status(500).json({ message: err.message });
      // })

      // Start the server
      http.createServer(app).listen(serverPort, function () {
        console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
        console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
      });

    });
  }
}

// Initialise all IOC services:
callServices('init')

// Ref. http://expressjs.com/en/guide/error-handling.html
const clientErrorMiddleware = (err: any, req: any, res: any, next: any) => {
  if (typeof (err) === 'string') {
    // custom application error
    res.status(400).json({ error: "Error", message: err });
    return;
  }
  if (err.name === 'ClientError') {
    const ce = <ClientError>err;
    res.status(ce.code).json(ce);
    return;
  }
  // default to 500 server error
  // return res.status(500).json({ message: err.message });
  next(err);
}
